# Open Source Project by Nevion Soft

Небольшая программа с графическим интерфейсом для автоматизации рутинных процессов, сделанная на Qt(qml) + python

1) Создание бекапов
2) Запуск приложений
3) Запуск прокси сервера

<div align="center">
  <img src="./img/preview.png" alt="logo" width="400" height="400">
</div> 

# Стек технологий

<div align="center" style="display: flex; align-items: center;">
  <img src="./img/qtlogo.png" alt="Qt Logo" width="100" height="100">
  <span style="margin: 0 10px; font-size: 24px;">+</span>
  <img src="./img/python-logo.png" alt="Python Logo" width="100" height="100">
</div>

## Установка из исходников

Для сборки из исходников потребутеся установить [Qt](https://www.qt.io/download-dev)

```bash
git clone git@gitlab.com:web4450122/scripts-menu.git

# Через qt открыть файл `python.project`
```

Установка зависимостей

```bash
pip install -r requirements.txt
```

Все пути в программе это хардкод, для корректной работы нужно выставить свои пути, или закомментировать компоненты

```bash
# Для рабты прокси сервера добавть в проект файл `config.json` и впишите туда данные своего сервера
# Если у вас его нет, тогда закомментируйте эти строчки в классе proxy.py

        # Загрузка конфигурации из файла config.json
        config_path = os.path.join(os.path.dirname(__file__), 'config.json')
        with open(config_path, 'r') as config_file:
            self.config = json.load(config_file)
```

## ССЫЛКИ

  [<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_tg.png?ref_type=heads" width="100">](https://t.me/ancient_nevionn)
  [<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_coffee.png?ref_type=heads" width="100">](https://www.donationalerts.com/r/nevion)

