import sys
from pathlib import Path
from PySide6.QtGui import QGuiApplication, QIcon
from PySide6.QtQml import QQmlApplicationEngine
from injector import Injector
from backupMusic import BackupMusic
from backupPass import BackupPass
from translater import Translater
from albion import Albion
from pythontoexe import Pytoexe
from lolminer import Miner
from proxy import Proxy


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)

    # Установка иконки приложения
    icon_path = "./img\\favicon.ico"
    app.setWindowIcon(QIcon(icon_path))

    engine = QQmlApplicationEngine()

    # Создание экземпляра объектов и регистрация их в качестве контекста
    injector = Injector()
    engine.rootContext().setContextProperty("injector", injector)

    backupMusic = BackupMusic()
    engine.rootContext().setContextProperty("backupMusic", backupMusic)

    backupPass = BackupPass()
    engine.rootContext().setContextProperty("backupPass", backupPass)

    translater = Translater()
    engine.rootContext().setContextProperty("translater", translater)

    albion = Albion()
    engine.rootContext().setContextProperty("albion", albion)

    pytoexe = Pytoexe()
    engine.rootContext().setContextProperty("pytoexe", pytoexe)

    miner = Miner()
    engine.rootContext().setContextProperty("miner", miner)

    proxy = Proxy()
    engine.rootContext().setContextProperty("proxy", proxy)

    qml_file = Path(__file__).resolve().parent / "main.qml"
    engine.load(qml_file)

    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec())
