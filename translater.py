# This Python file uses the following encoding: utf-8
import subprocess
import os
from PySide6.QtCore import QObject, Slot, Signal

class Translater(QObject):
    translateMessage = Signal(str)

    def __init__(self):
        super().__init__()

    def run_file(self, file_path):
        try:
            subprocess.Popen(file_path, shell=True, cwd=os.path.dirname(file_path))
            self.translateMessage.emit("переводчик запущен\n")
        except subprocess.CalledProcessError as e:
            print(f"error {file_path}: {e}")

    @Slot()
    def run_translater(self):
        translater_folder_path = r"N:\soft\translate\elka\elka.exe"
        self.run_file(translater_folder_path)
