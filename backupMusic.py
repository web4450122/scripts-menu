import os
import shutil
from PySide6.QtCore import QObject, Slot, Signal

class BackupMusic(QObject):
    backupMusicMessage = Signal(str)

    def __init__(self):
        super().__init__()

    @Slot()
    def files_transactions(self):
        music_folder_path = os.path.join(os.getenv('USERPROFILE'), 'Music')
        destination_folder_path = 'N:\music'

        if os.path.exists(destination_folder_path) and os.path.isdir(destination_folder_path):
            files = os.listdir(music_folder_path)
            self.backupMusicMessage.emit("получение списка файлов\n")
            for file_name in files:
                source_file_path = os.path.join(music_folder_path, file_name)
                destination_file_path = os.path.join(destination_folder_path, file_name)
                if os.path.isfile(source_file_path):
                    shutil.copy(source_file_path, destination_file_path)
                elif os.path.isdir(source_file_path):
                    shutil.copytree(source_file_path, destination_file_path)
            self.backupMusicMessage.emit("копирование..\n")
            self.backupMusicMessage.emit("музыка сохранена")
        else:
            self.backupMusicMessage.emit("папка назначения не найдена или не является директорией")

