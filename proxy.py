import subprocess
from PySide6.QtCore import QObject, Slot, Signal
import json
import os

class Proxy(QObject):
    proxyMessage = Signal(str)

    def __init__(self):
        super().__init__()
        # Загрузка конфигурации из файла config.json
        config_path = os.path.join(os.path.dirname(__file__), 'config.json')
        with open(config_path, 'r') as config_file:
            self.config = json.load(config_file)

    @Slot()
    def run_proxy(self):
        try:
            # Получение значений из конфигурации
            port = self.config["ssh_port"]
            host = self.config["ssh_host"]
            user = self.config["ssh_user"]

            process = subprocess.Popen(
                ["ssh", "-D", port, "-p", port, f"{user}@{host}"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True
            )

            # Чтение вывода команды
            stdout, stderr = process.communicate()

            if process.returncode == 0:
                self.proxyMessage.emit(f"Запуск прокси\n: {stdout}")
            else:
                self.proxyMessage.emit(f"Error executing command: {stderr}")

        except Exception as e:
            self.proxyMessage.emit(f"Exception occurred: {str(e)}")
