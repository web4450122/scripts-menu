# This Python file uses the following encoding: utf-8
import subprocess
import os
from PySide6.QtCore import QObject, Slot

class Pytoexe(QObject):
    def __init__(self):
        super().__init__()

    def run_file(self, file_path):
        try:
            subprocess.Popen(file_path, shell=True, cwd=os.path.dirname(file_path))
        except subprocess.CalledProcessError as e:
            print(f"error {file_path}: {e}")

    @Slot()
    def run_pytoexe(self):
        pythonToExe_folder_path = r"C:\Users\ghoul\AppData\Roaming\Python\Python311\Scripts\autopytoexe.exe"
        self.run_file(pythonToExe_folder_path)
