import os
import shutil
from PySide6.QtCore import QObject, Slot, Signal


class BackupPass(QObject):
    backupMessage = Signal(str)

    def __init__(self):
        super().__init__()

    @Slot()
    def run_backupPass(self):
        data_file_path = 'N:\\project\\Пароли.kdbx'
        destination_folder_path = 'E:\\backUp'

        if os.path.exists(destination_folder_path) and os.path.isdir(destination_folder_path):
            destination_file_path = os.path.join(destination_folder_path, os.path.basename(data_file_path))
            self.backupMessage.emit("директория найдена\n")
            if os.path.isfile(data_file_path):
                self.backupMessage.emit("копирование..\n")
                shutil.copy(data_file_path, destination_file_path)
                self.backupMessage.emit("файл успешно скопирован")
            else:
                self.backupMessage.emit("исходный файл не найден")
        else:
            self.backupMessage.emit("папка назначения не найдена или не является директорией")


