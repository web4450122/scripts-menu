# This Python file uses the following encoding: utf-8
import subprocess
import os
import time
from PySide6.QtCore import QObject, Slot

class Injector(QObject):
    def __init__(self):
        super().__init__()

    def run_file(self, file_path):
        try:
            subprocess.Popen(file_path, shell=True, cwd=os.path.dirname(file_path))
        except subprocess.CalledProcessError as e:
            print(f"error {file_path}: {e}")

    @Slot()
    def run_injector(self):
        migoto_folder_path = r"N:\genshinMods\3dmigoto-GIMI-for-playing-mods\3dmigoto\3DMigoto Loader.exe"
        gasnya_folder_path = r"N:\games\Genshin Impact\Genshin Impact game\GenshinImpact.exe"

        self.run_file(migoto_folder_path)
        time.sleep(4)
        self.run_file(gasnya_folder_path)
