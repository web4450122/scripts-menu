# This Python file uses the following encoding: utf-8
import subprocess
import os
from PySide6.QtCore import QObject, Slot

class Albion(QObject):
    def __init__(self):
        super().__init__()

    def run_file(self, file_path):
        try:
            subprocess.Popen(file_path, shell=True, cwd=os.path.dirname(file_path))
        except subprocess.CalledProcessError as e:
            print(f"error {file_path}: {e}")

    @Slot()
    def run_albion(self):
        pythonToExe_folder_path = r"N:\project\PyScript\albionbot\FishbotKEKW-master\FishbotKEKW.ahk"
        self.run_file(pythonToExe_folder_path)
