import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15

Item {
    height: 40
    width: 200
    RowLayout {
        anchors.centerIn: parent
        Text {
            id: time
            text: getCurrentTime()
            color: "yellow"
            font.pixelSize: font_size + 10
            font.family: "impact"
        }
    }
    Timer {
        interval: 1000
        running: true
        repeat: true
        onTriggered: {
            time.text = getCurrentTime()
        }
    }

    function getCurrentTime() {
        var date = new Date()
        var hours = date.getHours()
        var minutes = date.getMinutes()
        var seconds = date.getSeconds()

        hours = (hours < 10) ? "0" + hours : hours
        minutes = (minutes < 10) ? "0" + minutes : minutes
        seconds = (seconds < 10) ? "0" + seconds : seconds

        return hours + ":" + minutes + ":" + seconds
    }
}
