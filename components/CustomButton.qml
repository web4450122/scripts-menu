import QtQuick.Window
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15


Button {
    property string buttonText: ""
    property int _height: 40
    property int _width: 189
    property color bgColor: button_color
    property color borderColor: button_border_color

    background: Rectangle {
        color: bgColor
        border.color: borderColor
        implicitHeight: _height
        implicitWidth: _width
        radius: 5
    }
    contentItem: Text {
        text: buttonText
        color: "white"
        font.bold: true
        font.pixelSize: font_size
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
