import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15

Rectangle {
    id: terminal
    property bool _terminal_visible: terminal_visible
    property int _height: 0
    property int _width: 0

    function cleanTerminal() {
        terminalText.text = ""
    }

    height: _height
    width: _width
    z: 2
    color: "transparent"
    radius: 12
    visible: _terminal_visible
    clip: true
    Flickable {
        id: flickable
        anchors.fill: parent
        contentWidth: terminalText.width
        contentHeight: terminalText.height
        boundsBehavior: Flickable.StopAtBounds
        clip: true
        Text {
            id: terminalText
            text: ""
            color: "white"
            font.pixelSize: 18
            wrapMode: Text.WrapAnywhere
            width: flickable.width
            anchors {
                left: parent.left
                right: parent.right
                margins: 10
            }
        }
    }
    Connections {
        target: backupPass
        function onBackupMessage(logi) {
            terminalText.text += logi
        }
    }
    Connections {
        target: translater
        function onTranslateMessage(logi) {
            terminalText.text += logi
        }
    }
    Connections {
        target: backupMusic
        function onBackupMusicMessage(logi) {
            terminalText.text += logi
        }
    }
    Connections {
        target: proxy
        function onProxyMessage(logi) {
            terminalText.text += logi
        }
    }
}
