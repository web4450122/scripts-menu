# This Python file uses the following encoding: utf-8
import sys
import ctypes
import subprocess
from PySide6.QtCore import QObject, Slot

class Miner(QObject):
    def __init__(self):
        super().__init__()

    def run_file(self, file_path):
        try:
            if sys.platform.startswith('win'):
                # Запуск процесса с правами администратора
                ctypes.windll.shell32.ShellExecuteW(None, "runas", file_path, None, None, 1)
        except subprocess.CalledProcessError as e:
            print(f"error {file_path}: {e}")

    @Slot()
    def run_miner(self):
        miner_pyrin_folder_path = r"N:\soft\crypto\lolMiner_v1.86_Win64\1.86\mine_pyrin.bat"
        self.run_file(miner_pyrin_folder_path)
