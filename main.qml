import QtQuick.Window
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Shapes 1.15
import "components"

Window {
    id: window
    property int font_size: 18
    property color button_color: "#2375D6"
    property color button_border_color: "#1FC29D"
    property bool terminal_visible: false
    height:  terminal_visible ? 570 : 480
    width: 640
    maximumHeight: 570
    minimumHeight: 480
    maximumWidth: 640
    minimumWidth: 640
    visible: true
    title: qsTr("Каталог скриптов")
    flags: Qt.Window | Qt.CustomizeWindowHint | Qt.WindowTitleHint | Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint

    Rectangle {
        id: root
        height: 480
        width: 640
        Image {
            source: "img/cyber.png"
            anchors.fill: parent
        }
        DisplayTime {
            id: time
            anchors {
                bottom: blur.top
                bottomMargin: 10
                horizontalCenter: parent.horizontalCenter
            }
        }
        Rectangle {
            id: blur
            z: 1
            color: "#660066"
            opacity: 0.3
            border.width: 2
            height:  280
            width:  540
            radius: 8
            anchors.centerIn: parent
        }
        Rectangle {
            id: listForButtons
            z: 10
            color: "transparent"
            border.color: "#660066"
            border.width: 1
            radius: 8
            height: 280 /*root.height - 200*/
            width: 540 /*root.width - 100*/
            anchors.centerIn: parent
            RowLayout {
                id: rowLayout
                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                    rightMargin: 20
                    leftMargin: 20
                    topMargin: 20
                }
                ColumnLayout {
                    id: leftColumn
                    Layout.alignment: Qt.AlignLeft
                    spacing: 20
                    CustomButton {
                        id: buttonInjector
                        buttonText: qsTr("Запустить инжектор")
                        onClicked: {
                            terminal.cleanTerminal()
                            injector.run_injector()
                        }
                    }
                    CustomButton {
                        id: buttonTranslater
                        buttonText: qsTr("Переводчик елка")
                        onClicked: {
                            terminal.cleanTerminal()
                            translater.run_translater()
                        }
                    }
                    CustomButton {
                        id: buttonPythonToExe
                        buttonText: qsTr("Сборщик python")
                        onClicked: {
                            terminal.cleanTerminal()
                            pytoexe.run_pytoexe()
                        }
                    }
                    CustomButton {
                        id: buttonProxy
                        buttonText: qsTr("Прокси")
                        onClicked: {
                            terminal.cleanTerminal()
                            proxy.run_proxy()
                        }
                    }
                }
                ColumnLayout {
                    id: rightColumn
                    Layout.alignment: Qt.AlignRight
                    spacing: 20
                    CustomButton {
                        id: buttonBackupMusic
                        buttonText: qsTr("Бекап музыки")
                        onClicked: {
                            terminal.cleanTerminal()
                            backupMusic.files_transactions()
                        }
                    }
                    CustomButton {
                        id: buttonAlbion
                        buttonText: qsTr("Альбион")
                        onClicked: {
                            terminal.cleanTerminal()
                            albion.run_albion()
                        }
                    }
                    CustomButton {
                        id: buttonMiner
                        buttonText: qsTr("Майнер")
                        onClicked: {
                            terminal.cleanTerminal()
                            miner.run_miner()
                        }
                    }
                    CustomButton {
                        id: buttonSaveDataPass
                        buttonText: qsTr("Бекап паролей")
                        onClicked: {
                            terminal.cleanTerminal()
                            backupPass.run_backupPass()
                        }
                    }
                }
            }
        }
        Rectangle {
            id: blurForTerminal
            z: 1
            color: "#422F53"
            opacity: 0.7
            border.width: 2
            height: 140
            width: listForButtons.width
            radius: 8
            visible: terminal_visible
            anchors {
                top: listForButtons.bottom
                horizontalCenter: listForButtons.horizontalCenter
                topMargin: 14
            }
        }
        Terminal {
            id: terminal
            _height: 140
            _width: listForButtons.width
            anchors {
                top: listForButtons.bottom
                horizontalCenter: listForButtons.horizontalCenter
                topMargin: 14
            }
        }
        Rectangle {
            id: optionBottomPanel
            height: 140
            width: root.width
            color: "#292729"
            visible: terminal_visible
            anchors {
                top: root.bottom
                horizontalCenter: root.horizontalCenter
            }
            Row {
                anchors.fill: parent
                Repeater {
                    model: 7
                    Image {
                        source: "img/rombY.png"
                        width: 100
                        height: 100
                    }
                }
            }
        }
        CustomButton {
            id: buttonToOpenTerminal
            _height: 20
            _width: 50
            bgColor: "black"
            borderColor: "white"
            buttonText: qsTr(">_")
            anchors {
                bottom: listForButtons.top
                right: listForButtons.right
                rightMargin: 20
                bottomMargin: 10
            }
            onClicked: {
                terminal_visible = !terminal_visible
            }
        }
    }
}
